var currentView = new PlatformNumberOverrideView();

$(document).ready(function() {
    $('body').click(function () {
        $('#stationDropDown').hide();
    });

    $('#finalize').hide();

    currentView.getPage();

    $('.scroll-container').scroll(function() {
        var scrollTop = $('.scroll-container').scrollTop();
        var docHeight = $('#scrollView').height();
        var winHeight = $('.scroll-container').height();
        var scrollPercent = (scrollTop) / (docHeight - winHeight);
        var scrollPercentRounded = Math.round(scrollPercent*100);

        if (!currentView.requestIsInProgress && !currentView.allPagesDowloaded && scrollPercentRounded > 80) {
            requestIsInProgress = true;
            $('#loading').show();
            currentView.getPage();
        }
    });
});






