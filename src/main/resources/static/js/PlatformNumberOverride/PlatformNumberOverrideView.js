class PlatformNumberOverrideView {
    constructor() {
        this.pageSize = 25;
        this.pageIndex = 0;
        this.requestIsInProgress = false;
        this.allPagesDowloaded = false;
        this.allCachedElementsCollection = new Map();
        this.stationDropDownCollection = new Map();
        this.selectedElement = null;
    }

    static makeTableRow(platformNumberOverride) {
        return "<tr id='" + platformNumberOverride.id + "' onclick='currentView.updateDetails("+ platformNumberOverride.id +")'>" +
            "<td name>" + platformNumberOverride.station.name + "</td>" +
            "<td op>" + platformNumberOverride.operationalPlatform + "</td>" +
            "<td pp>" + platformNumberOverride.publishedPlatform + "</td>" +
            "<td comment>" + platformNumberOverride.comment + "</td></tr>"
    }

    static makeStationDropDownRow(station) {
        return "<button class='dropdown-item' type='button' onclick='currentView.applyDropDownValue(" + station.id + ", \"" + station.name + "\", " + station.nlc + ", \""+ station.tiploc +"\")'>" +
            "<span class='dropdown-subitem'>" + station.name + "</span>" +
            "<span class='dropdown-subitem-nlc'><span class='dropdown-subitem-label'>Nlc:</span> " + station.nlc + "</span>" +
            "<span class='dropdown-subitem'><span class='dropdown-subitem-label'>Tiploc:</span> " + station.tiploc + "</span>" +
            "</button>";
    }

    loaded() {
        var firstItemKey = currentView.allCachedElementsCollection.keys().next().value;
        updateDetails(firstItemKey);
    }

    getPage() {
        var that = this;
        var search = $('#search').val();
        $.ajax({
            url: '/platformNumberOverride/pageByComment?page=' + this.pageIndex + '&size=' + this.pageSize + '&comment=' + search,
            contentType: "application/json",
            dataType: "json",
            success: function(responseObject) {
                responseObject.content.forEach(function(responseItem){
                    var station = new Station(
                        responseItem.station.id,
                        responseItem.station.name,
                        responseItem.station.lastModified,
                        responseItem.station.nlc,
                        responseItem.station.tiploc);

                    var platformNumberOverride = new PlatformNumberOverride(
                        responseItem.id,
                        station,
                        responseItem.operationalPlatform,
                        responseItem.publishedPlatform,
                        responseItem.comment,
                        responseItem.lastModified);

                    // add to local cache/db
                    that.allCachedElementsCollection.set(platformNumberOverride.id, platformNumberOverride);
                    var html = PlatformNumberOverrideView.makeTableRow(platformNumberOverride);
                    $('#scrollView').append(html);
                });

                // select first item in details view
                if ($('#station').val().length < 1) {
                    var firstItemKey = that.allCachedElementsCollection.keys().next().value;
                    that.updateDetails(firstItemKey);
                }

                $('#loading').hide();
                that.pageIndex++;
                that.allPagesDowloaded = responseObject.last;
                that.requestIsInProgress = false;
            }
        });
    }

    updateDetails(id) {
        this.selectedElement = this.allCachedElementsCollection.get(id);

        $('#stationId').val(this.selectedElement.station.id);
        $('#station').val(this.selectedElement.station.name);
        $('#nlc').val(this.selectedElement.station.nlc);
        $('#tiploc').val(this.selectedElement.station.tiploc);
        $('#operationalPlatform').val(this.selectedElement.operationalPlatform);
        $('#publishedPlatform').val(this.selectedElement.publishedPlatform);
        $('#comment').val(this.selectedElement.comment);
    }

    update() {
        this.selectedElement.station.id = $('#stationId').val();
        this.selectedElement.station.name = $('#station').val();
        this.selectedElement.operationalPlatform = $('#operationalPlatform').val();
        this.selectedElement.publishedPlatform = $('#publishedPlatform').val();
        this.selectedElement.comment = $('#comment').val();
        var that = this;

        $.ajax({
            type: "POST",
            url: '/platformNumberOverride/update/' + this.selectedElement.id,
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(this.selectedElement),
            success: function(response) {
                that.syncViews();
            },
            error: function(response) {
                console.log(response);
            }
        });
    }

    searchForStation() {
        var stationSearchKey = $('#station').val();
        if (stationSearchKey.length < 2)
            return;

        var that = this;

        $.ajax({
            url: '/station/search?key=' + stationSearchKey + '&limit=15',
            contentType: "application/json",
            dataType: "json",
            success: function(responseObject) {
                // clear dropdown
                that.stationDropDownCollection.clear();
                $('#stationDropDown').html("");

                responseObject.forEach(function(responseItem){
                    var station = new Station(
                        responseItem.id,
                        responseItem.name,
                        responseItem.lastModified,
                        responseItem.nlc,
                        responseItem.tiploc);

                    that.stationDropDownCollection.set(station.id, station);
                    var html = PlatformNumberOverrideView.makeStationDropDownRow(station);
                    $('#stationDropDown').append(html);
                });
                $('#stationDropDown').show();
            }
        });
    }

    search() {
        this.allCachedElementsCollection.clear();
        $('#items').html('');

        this.pageIndex = 0;
        this.getPage();
    }

    applyDropDownValue(stationId, stationName, stationNlc, stationTiploc) {
        $('#stationDropDown').hide();

        $('#stationId').val(stationId);
        $('#station').val(stationName);
        $('#nlc').val(stationNlc);
        $('#tiploc').val(stationTiploc);
    }

    // synchronize details view with list view
    syncViews() {
        var id = this.selectedElement.id;
        $('#items [id=' + id + '] [name]').html(this.selectedElement.station.name);
        $('#items [id=' + id + '] [op]').html(this.selectedElement.operationalPlatform);
        $('#items [id=' + id + '] [pp]').html(this.selectedElement.publishedPlatform);
        $('#items [id=' + id + '] [comment]').html(this.selectedElement.comment);
    }

    add() {
        $('#updateAdd').hide();

        $('#stationId').val('');
        $('#station').val('');
        $('#nlc').val('');
        $('#tiploc').val('');
        $('#operationalPlatform').val('');
        $('#publishedPlatform').val('');
        $('#comment').val('');

        $('#station').focus();

        $('#finalize').show();
    }

    save() {
        if ($('#station').val().length < 2 || $('#operationalPlatform').val().length < 2 || $('#publishedPlatform').val().length < 2) {
            alert("Station, Operational or Publishing platforms is invalid");
            return;
        }

        this.selectedElement.station.id = $('#stationId').val();
        this.selectedElement.station.name = $('#station').val();
        this.selectedElement.operationalPlatform = $('#operationalPlatform').val();
        this.selectedElement.publishedPlatform = $('#publishedPlatform').val();
        this.selectedElement.comment = $('#comment').val();
        var that = this;

        $.ajax({
            type: "POST",
            url: '/platformNumberOverride/create',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(this.selectedElement),
            success: function(response) {
                that.syncViews();
            },
            error: function(response) {
                console.log(response);
            }
        });

        $('#finalize').hide();
        $('#updateAdd').show();
    }

    cancel() {
        $('#finalize').hide();
        $('#updateAdd').show();
    }
}

