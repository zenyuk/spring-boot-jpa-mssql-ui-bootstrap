class Station {
    constructor(id, name, lastModified, nlc, tiploc) {
        this.id = id;
        this.name = name;
        this.lastModified = lastModified;
        this.nlc = nlc;
        this.tiploc = tiploc;
    }
}