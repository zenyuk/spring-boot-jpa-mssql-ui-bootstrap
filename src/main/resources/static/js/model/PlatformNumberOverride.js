class PlatformNumberOverride {
    constructor(id, station, operationalPlatform, publishedPlatform, comment, lastModified) {
        this.id = id;
        // custom type; see Station.js
        this.station = station;
        this.operationalPlatform = operationalPlatform;
        this.publishedPlatform = publishedPlatform;
        this.comment = comment;
        this.lastModified = lastModified;
    }
}