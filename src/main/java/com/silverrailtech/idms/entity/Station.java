package com.silverrailtech.idms.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Station {
    @Id
    private int id;

    @Column(columnDefinition = "nvarchar(8)")
    private String tiploc;

    @Column(columnDefinition = "nchar(4)")
    private String nlc;

    @Column(columnDefinition = "nchar(3)")
    private String crs;

    @Column(columnDefinition = "nvarchar(max)")
    private String name;

    @Column(columnDefinition = "bit")
    private boolean isTimetabled;

    @Column(columnDefinition = "bit")
    private boolean attendedTIS;

    @Column(columnDefinition = "datetime")
    private Date lastModified;
}
