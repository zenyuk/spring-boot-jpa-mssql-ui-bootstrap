package com.silverrailtech.idms.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class PlatformNumberOverride {
    @Id
    private int id;

    @ManyToOne
    @JoinColumn(name = "StationId")
    private Station station;

    @Column(columnDefinition = "nvarchar(3)")
    private String operationalPlatform;

    @Column(columnDefinition = "nvarchar(3)")
    private String publishedPlatform;

    @Column(columnDefinition = "nvarchar(max)")
    private String comment;

    @Column(columnDefinition = "datetime")
    private Date lastModified;
}
