package com.silverrailtech.idms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootApplication
public class IdmsApplication  {

	public static void main(String[] args) {
		SpringApplication.run(IdmsApplication.class, args);
	}
}
