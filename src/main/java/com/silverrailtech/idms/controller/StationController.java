package com.silverrailtech.idms.controller;

import com.silverrailtech.idms.entity.Station;
import com.silverrailtech.idms.repository.StationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/station")
public class StationController {
    @Autowired
    StationRepository stationRepository;

    @GetMapping("/{stationId}")
    public Station getStation(@PathVariable int stationId) {
        return stationRepository.findOne(stationId);
    }

    @GetMapping("/searchByName")
    public List<Station> findStationByName(@RequestParam String name) {
        return stationRepository.findAllByNameContaining(name);
    }

    @GetMapping("/searchByTiploc}")
    public List<Station> findStationByTiploc(@RequestParam String tiploc) {
        return stationRepository.findAllByTiplocContaining(tiploc);
    }

    @GetMapping("/searchByNlc")
    public List<Station> findStationByNlc(@RequestParam String nlc) {
        return stationRepository.findAllByNlcContaining(nlc);
    }

    @GetMapping("/search")
    public List<Station> findStationByMultipleFields(@RequestParam String key, @RequestParam(required = false) Integer limit) {

        List<Station> result = stationRepository.findAllByNameContainingOrNlcContainingOrTiplocContaining(key, key, key);

        if (limit != null && limit > 0)
            result = result.stream()
                .limit(limit)
                .collect(Collectors.toList());

        return result;
    }
}
