package com.silverrailtech.idms.controller;

import com.silverrailtech.idms.entity.PlatformNumberOverride;
import com.silverrailtech.idms.entity.Station;
import com.silverrailtech.idms.repository.PlatformNumberOverrideRepository;
import com.silverrailtech.idms.repository.StationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/platformNumberOverride")
public class PlatformNumberOverrideController {
    @Autowired
    PlatformNumberOverrideRepository platformNumberOverrideRepository;

    @Autowired
    StationRepository stationRepository;

    @GetMapping("/{id}")
    public PlatformNumberOverride getPlatformNumberOverride(@PathVariable int id) {
        return platformNumberOverrideRepository.findOne(id);
    }

    @GetMapping("/page")
    public Page<PlatformNumberOverride> getPlatformNumberOverride(Pageable pageable) {
        return platformNumberOverrideRepository.findAll(pageable);
    }

    @GetMapping("/pageByComment")
    public Page<PlatformNumberOverride> getPlatformNumberOverride(Pageable pageable, @RequestParam String comment) {
        if (comment == null || comment.length() == 0)
            return platformNumberOverrideRepository.findAll(pageable);

        return platformNumberOverrideRepository.findAllByCommentContaining(pageable, comment);
    }

    @PostMapping("/update/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public PlatformNumberOverride updatePlatformNumberOverride(@PathVariable int id, @RequestBody PlatformNumberOverride updated) {
        PlatformNumberOverride original = platformNumberOverrideRepository.findOne(id);
        if(updated.getStation().getId() != original.getStation().getId()) {
            Station station = stationRepository.findOne(updated.getStation().getId());
            original.setStation(station);
        }
        original.setOperationalPlatform(updated.getOperationalPlatform());
        original.setPublishedPlatform(updated.getPublishedPlatform());
        original.setComment(updated.getComment());
        return platformNumberOverrideRepository.save(original);
    }

    @PostMapping("/create")
    @ResponseStatus(value = HttpStatus.OK)
    public PlatformNumberOverride createPlatformNumberOverride(@RequestBody PlatformNumberOverride newOne) {
        return platformNumberOverrideRepository.save(newOne);
    }
}
