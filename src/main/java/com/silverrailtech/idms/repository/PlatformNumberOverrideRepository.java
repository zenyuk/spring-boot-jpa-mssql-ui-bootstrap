package com.silverrailtech.idms.repository;

import com.silverrailtech.idms.entity.PlatformNumberOverride;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PlatformNumberOverrideRepository extends PagingAndSortingRepository<PlatformNumberOverride, Integer> {
    Page<PlatformNumberOverride> findAllByCommentContaining(Pageable pageable, String comment);
}
