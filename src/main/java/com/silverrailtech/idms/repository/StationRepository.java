package com.silverrailtech.idms.repository;

import com.silverrailtech.idms.entity.Station;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StationRepository extends CrudRepository<Station, Integer> {
    List<Station> findAllByNameContaining(String name);

    List<Station> findAllByTiplocContaining(String tiploc);

    List<Station> findAllByNlcContaining(String nlc);

    List<Station> findAllByNameContainingOrNlcContainingOrTiplocContaining(String name, String nlc, String tiploc);
}
